//
//  KWEditTaskViewController.h
//  Overdue
//
//  Created by RJ Militante on 2/3/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KWTask.h"

@protocol KWEditTaskViewControllerDelegate <NSObject>

-(void)didUpdateTask;

@end

@interface KWEditTaskViewController : UIViewController<UITextViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic)KWTask *task;
@property (weak, nonatomic) id <KWEditTaskViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)saveBarButtonItemPressed:(UIBarButtonItem *)sender;

@end
