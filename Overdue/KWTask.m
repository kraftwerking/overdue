//
//  KWTask.m
//  Overdue
//
//  Created by RJ Militante on 2/3/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "KWTask.h"

@implementation KWTask

-(id)initWithData:(NSDictionary *)data
{
    self = [super init];
    if(self){
        self.title = data[TASK_TITLE];
        self.descr = data[TASK_DESCRIPTION];
        self.date = data[TASK_DATE];
        self.isCompletion = [data[TASK_COMPLETION] boolValue];
        
    }
    return self;
}

-(id)init
{
    self = [self initWithData:nil];
    return self;
}

@end
