//
//  KWDetailTaskViewController.h
//  Overdue
//
//  Created by RJ Militante on 2/3/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KWTask.h"
#import "KWEditTaskViewController.h"

@protocol KWDetailTaskViewControllerDelegate <NSObject>

-(void)updateTask;

@end

@interface KWDetailTaskViewController : UIViewController<KWEditTaskViewControllerDelegate>
@property (strong, nonatomic) KWTask *task;

@property (weak, nonatomic) id <KWDetailTaskViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *detailLabel;
- (IBAction)editBarButtonItemPressed:(UIBarButtonItem *)sender;


@end
