//
//  KWTask.h
//  Overdue
//
//  Created by RJ Militante on 2/3/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KWTask : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *descr;
@property (strong, nonatomic) NSDate   *date;
@property (nonatomic) BOOL isCompletion;

-(id)initWithData:(NSDictionary *)data;

@end
