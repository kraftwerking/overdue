//
//  KWAddTaskViewController.h
//  Overdue
//
//  Created by RJ Militante on 2/3/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KWTask.h"

@protocol KWAddTaskViewControllerDelegate <NSObject>

-(void)didCancel;
-(void)didAddTask:(KWTask *)task;

@end

@interface KWAddTaskViewController : UIViewController<UITextViewDelegate, UITextFieldDelegate>

@property (weak,nonatomic) id<KWAddTaskViewControllerDelegate> delegate;


@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;

- (IBAction)addTaskButtonPressed:(UIButton *)sender;
- (IBAction)cancelButtonPressed:(UIButton *)sender;

@end
