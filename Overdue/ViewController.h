//
//  ViewController.h
//  Overdue
//
//  Created by RJ Militante on 2/3/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KWAddTaskViewController.h"
#import "KWDetailTaskViewController.h"

@interface ViewController : UIViewController <KWAddTaskViewControllerDelegate, UITableViewDataSource, UITableViewDelegate,KWDetailTaskViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)reorderBarButtonItemPressed:(UIBarButtonItem *)sender;
- (IBAction)addTaskBarButtonItemPressed:(UIBarButtonItem *)sender;
@property (strong, nonatomic) NSMutableArray *taskObjects;

@end

